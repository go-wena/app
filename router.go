package app

import (
	"gitee.com/go-wena/app/internal/mux"
)

var router = mux.New()

type (
	Context     = mux.Context
	HandlerFunc = func(ctx *Context)
)

func HandleGenerated(method string, pattern string, handler HandlerFunc) {
	router.Handle(method, pattern, handler)
}
