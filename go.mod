module gitee.com/go-wena/app

go 1.16

require (
	gitee.com/go-errors/errors v0.0.1
	gitee.com/go-wena/env v0.0.1
	github.com/ghodss/yaml v1.0.0
	github.com/go-chi/chi v1.5.4
	github.com/gomodule/redigo v1.8.4
	github.com/segmentio/go-snakecase v1.2.0
	github.com/takama/daemon v1.0.0
	github.com/valyala/fastjson v1.6.3
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68
	gorm.io/driver/mysql v1.1.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.10
)
