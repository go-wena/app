package app

import (
	"context"
	"gitee.com/go-wena/app/internal/redis"
)

var Redis = &redisService{}

type redisService struct{}

func (s *redisService) Get(ctx context.Context) redis.Connection {
	return redis.Get(ctx)
}

func (s *redisService) GetStats() redis.Stats {
	return redis.GetStats()
}

func (s *redisService) Ping(ctx context.Context) error {
	return redis.Ping(ctx)
}
