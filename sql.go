package app

import (
	"context"

	"gitee.com/go-wena/app/internal/sql"
	"gorm.io/gorm"
)

var SQL = &sqlService{}

type sqlService struct{}

func (s *sqlService) Get(ctx context.Context) *gorm.DB {
	return sql.Get(ctx)
}

func (s *sqlService) GetStats() sql.Stats {
	return sql.GetStats()
}

func (s *sqlService) Ping(ctx context.Context) error {
	return sql.Ping(ctx)
}
