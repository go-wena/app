package app

import (
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

var (
	name        = ""
	description = ""
	version     = "0.0.0"
	buildTime   time.Time

	workDIR, _    = os.Getwd()
	executable, _ = os.Executable()
)

func Name() string {
	if name == "" {
		name = filepath.Base(executable)
	}
	return name
}

func Description() string {
	if description == "" {
		return "-"
	}
	return description
}

func Version() string {
	if version == "" {
		version = "0.0.0"
	}
	return version
}

func BuildTime() time.Time {
	return buildTime
}

func WorkDIR() string {
	return workDIR
}

func Executable() string {
	return executable
}

func headerPrint() {
	log.Printf("%-12s %s\n", "Name:", Name())
	log.Printf("%-12s %s\n", "Description:", Description())
	log.Printf("%-12s %s\n", "Version:", Version())
	log.Printf("%-12s %s\n", "Build:", BuildTime().Format(time.RFC3339))
	log.Printf("%-12s %s.%s\n", "Platform:", strings.Title(runtime.GOOS), strings.Title(runtime.GOARCH))
	log.Printf("%-12s %s\n", "Binary:", Executable())
	log.Printf("%-12s %s\n", "WorkDIR:", WorkDIR())
}
