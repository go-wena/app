package app

import (
	"context"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitee.com/go-wena/app/internal/hs"
	"gitee.com/go-wena/app/internal/redis"
	"gitee.com/go-wena/app/internal/sql"
	"gitee.com/go-wena/env"
)

func Execute(appName, appVersion, appDescription string, appBuildTime time.Time, runs ...func()) {
	name = appName
	description = appDescription
	version = appVersion
	buildTime = appBuildTime

	headerPrint()
	env.Auto()

	for _, run := range runs {
		run()
	}

	var ctx, cancel = signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM, os.Kill, syscall.SIGUSR1, syscall.SIGUSR2)
	defer cancel()

	sql.Auto()
	redis.Auto()

	if env.GetBool("router.enabled", true) {
		hs.Start(ctx, router.Handler())
	}
}
