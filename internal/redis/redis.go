package redis

import (
	"context"

	"gitee.com/go-errors/errors"
)

func Get(ctx context.Context) Connection {
	return defaultInstance.GetContext(ctx)
}

func GetStats() Stats {
	return defaultInstance.Stats()
}

func Ping(ctx context.Context) error {
	reply, err := Get(ctx).Do("PING")
	if err != nil {
		return errors.Errorf("redis: ping: %w", err)
	}
	if s, _ := reply.(string); s != "PONG" {
		return errors.Errorf("redis: ping: %v", reply)
	}
	return nil
}
