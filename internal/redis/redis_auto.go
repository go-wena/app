package redis

import (
	"context"
	"log"

	"gitee.com/go-errors/errors"
	"gitee.com/go-wena/app/internal/redis/internal"
	"gitee.com/go-wena/env"
)

var defaultInstance = internal.ErrPool(errors.New("redis: not init"))

type (
	Connection = internal.Connection
	Pool       = internal.Pool
	Stats      = internal.Stats
)

func Auto() {
	if !env.GetBool("redis.enabled") {
		defaultInstance = internal.ErrPool(errors.New("redis: disabled"))
		log.Printf("%v\n", defaultInstance.Err())
		return
	}

	dsn := env.GetString("redis.dsn")
	log.Printf("redis: connect: %s\n", dsn)
	defaultInstance = internal.New(dsn)

	if err := Ping(context.Background()); err != nil {
		log.Printf("redis: ping error: %v\n", err)
	}
}
