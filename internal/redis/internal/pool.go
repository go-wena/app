package internal

import (
	"context"
	"time"
)

type Pool interface {
	GetContext(ctx context.Context) Connection
	Stats() Stats
	Err() error
}

type Connection interface {
	// Close closes the connection.
	Close() error

	// Err returns a non-nil value when the connection is not usable.
	Err() error

	// Do sends a command to the server and returns the received reply.
	Do(commandName string, args ...interface{}) (reply interface{}, err error)

	// Send writes the command to the client's output buffer.
	Send(commandName string, args ...interface{}) error

	// Flush flushes the output buffer to the Redis server.
	Flush() error

	// Receive receives a single reply from the Redis server
	Receive() (reply interface{}, err error)
}

type Stats struct {
	// ActiveCount is the number of connections in the pool. The count includes
	// idle connections and connections in use.
	ActiveCount int `json:"active_count,omitempty"`
	// IdleCount is the number of idle connections in the pool.
	IdleCount int `json:"idle_count,omitempty"`

	// WaitCount is the total number of connections waited for.
	// This value is currently not guaranteed to be 100% accurate.
	WaitCount int64 `json:"wait_count,omitempty"`

	// WaitDuration is the total time blocked waiting for a new connection.
	// This value is currently not guaranteed to be 100% accurate.
	WaitDuration time.Duration `json:"wait_duration,omitempty"`
}
