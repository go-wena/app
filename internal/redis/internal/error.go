package internal

type errorConn struct{ err error }

func (ec errorConn) Do(string, ...interface{}) (interface{}, error) { return nil, ec.err }
func (ec errorConn) Send(string, ...interface{}) error              { return ec.err }
func (ec errorConn) Err() error                                     { return ec.err }
func (ec errorConn) Close() error                                   { return nil }
func (ec errorConn) Flush() error                                   { return ec.err }
func (ec errorConn) Receive() (interface{}, error)                  { return nil, ec.err }
