package internal

import (
	"context"

	"gitee.com/go-errors/errors"
	"github.com/gomodule/redigo/redis"
)

func ErrPool(err error) Pool {
	return &pool{err: err}
}

type pool struct {
	*redis.Pool
	err error
}

func (p *pool) Err() error {
	return p.err
}

func (p *pool) GetContext(ctx context.Context) Connection {
	if p.Pool == nil {
		return errorConn{err: errors.New("redis pool has not init")}
	}
	conn, err := p.Pool.GetContext(ctx)
	if err != nil {
		return errorConn{err: err}
	}
	return conn
}

func (p *pool) Stats() Stats {
	if p.Pool == nil {
		return Stats{}
	}
	return Stats(p.Pool.Stats())
}

var _ Pool = (*pool)(nil)
