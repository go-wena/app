package sql

import (
	"context"

	"gitee.com/go-errors/errors"
	"gorm.io/gorm"
)

func Get(ctx context.Context) *gorm.DB {
	if defaultInstance == nil {
		return &gorm.DB{Error: errors.New("no source")}
	}
	return defaultInstance.WithContext(ctx)
}

func GetStats() Stats {
	db, err := Get(context.Background()).DB()
	if err != nil {
		return Stats{}
	}
	return Stats(db.Stats())
}

func Ping(ctx context.Context) error {
	sdb, err := Get(ctx).DB()
	if err != nil {
		return errors.Errorf("ping: %w", err)
	}
	if sdb == nil {
		return errors.Errorf("ping: no sql instance found")
	}
	if err := sdb.PingContext(ctx); err != nil {
		return errors.Errorf("ping: %w", err)
	}
	return nil
}
