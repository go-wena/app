// +build sqlite

package internal

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func dialSqlite(dsn string) gorm.Dialector {
	return sqlite.Open(dsn)
}
