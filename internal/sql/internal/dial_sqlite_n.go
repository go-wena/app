// +build !sqlite

package internal

import (
	"gorm.io/gorm"
)

func dialSqlite(string) gorm.Dialector {
	return nil
}
