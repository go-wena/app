// +build postgres

package internal

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func dialPostgres(dsn string) gorm.Dialector {
	return postgres.Open(dsn)
}
