// +build mysql

package internal

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func dialMysql(dsn string) gorm.Dialector {
	return mysql.Open(dsn)
}
