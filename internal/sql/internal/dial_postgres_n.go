// +build !postgres

package internal

import (
	"gorm.io/gorm"
)

func dialPostgres(string) gorm.Dialector {
	return nil
}
