// +build !mysql

package internal

import (
	"gorm.io/gorm"
)

func dialMysql(string) gorm.Dialector {
	return nil
}
