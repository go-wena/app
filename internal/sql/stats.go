package sql

import "time"

// Stats contains database statistics.
type Stats struct {
	MaxOpenConnections int `json:"max_open_connections,omitempty"` // Maximum number of open connections to the database.

	// Pool Status
	OpenConnections int `json:"open_connections,omitempty"` // The number of established connections both in use and idle.
	InUse           int `json:"in_use,omitempty"`           // The number of connections currently in use.
	Idle            int `json:"idle,omitempty"`             // The number of idle connections.

	// Counters
	WaitCount         int64         `json:"wait_count,omitempty"`           // The total number of connections waited for.
	WaitDuration      time.Duration `json:"wait_duration,omitempty"`        // The total time blocked waiting for a new connection.
	MaxIdleClosed     int64         `json:"max_idle_closed,omitempty"`      // The total number of connections closed due to SetMaxIdleConns.
	MaxIdleTimeClosed int64         `json:"max_idle_time_closed,omitempty"` // The total number of connections closed due to SetConnMaxIdleTime.
	MaxLifetimeClosed int64         `json:"max_lifetime_closed,omitempty"`  // The total number of connections closed due to SetConnMaxLifetime.
}
