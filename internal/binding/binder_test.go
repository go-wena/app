package binding

import (
	"encoding/json"
	"log"
	"mime/multipart"
	"net/url"
	"testing"
	"time"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

type Ami struct {
	Father string
	Mather *string
}

type Who struct {
	Name     *string
	Age      int
	Weight   float64
	Birthday time.Time
	Profile  map[string]int
	Tags     []string
	Families []*Ami
}

func createFormSource(log func(args ...interface{})) Source {
	formV := url.Values{}
	formV.Add("Name", "wena")
	formV.Add("Age", "39")
	formV.Add("Weight", "75.5")
	formV.Add("Birthday", time.Now().Format(time.RFC3339))
	formV.Add("Profile[lat]", "110")
	formV.Add("Profile[lon]", "52")
	formV.Add("Tags[1]", "t1")
	formV.Add("Tags[2]", "t2")
	formV.Add("Tags[3]", "t0")
	formV.Add("Families[1].Mather", "mama1")
	formV.Add("Families[1].Father", "father1")
	formV.Add("Families[2].Father", "father0")
	formV.Add("Families[2].Mather", "mama0")

	log(formV.Encode())
	return multiSource(&multipart.Form{Value: formV})
}

func js(v interface{}) string {
	s, _ := json.MarshalIndent(v, "", "  ")
	return b2s(s)
}

func TestBind(t *testing.T) {
	src := createFormSource(t.Log)

	var who Who
	if err := (binder{}).Bind(src, &who); err != nil {
		t.Logf("%+v", err)
	}
	if who.Name != nil {
		t.Logf("Name: %s\n", *who.Name)
	}
	t.Logf("Age: %d\n", who.Age)
	t.Logf("Birthday: %s\n", who.Birthday)

	t.Log("\n" + js(who))
}

func BenchmarkBind(b *testing.B) {
	b.ReportAllocs()
	src := createFormSource(b.Log)
	for i := 0; i < b.N; i++ {
		if err := (binder{}).Bind(src, &Who{}); err != nil {
			b.Fatalf("%+v", err)
		}
	}
}
