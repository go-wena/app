package binding

import (
	"mime/multipart"
	"time"
)

type Source interface {
	Get(key string) (V Source, err error)
	Map(iter func(key string, valuer Source))
	Array(iter func(index int, valuer Source) bool, beforeFs ...func(size int))
	String() (s string, err error)
	Int() (i int64, err error)
	Uint() (u uint64, err error)
	Float() (f float64, err error)
	Bool() (b bool, err error)
	Time()(t time.Time, err error)
	Duration()(d time.Duration, err error)
	File() (f *multipart.FileHeader, err error)
	Files() (f []*multipart.FileHeader, err error)
}
