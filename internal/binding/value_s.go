package binding

import (
	"mime/multipart"
	"time"

	"gitee.com/go-errors/errors"
	"github.com/valyala/fastjson/fastfloat"
)

type stringValue string

func (v stringValue) Get(string) (V Source, err error) {
	return nil, ErrNoValue
}

func (v stringValue) Map(func(key string, valuer Source)) {}

func (v stringValue) Array(func(index int, valuer Source) bool, ...func(size int)) {}

func (v stringValue) String() (s string, err error) {
	return string(v), nil
}

func (v stringValue) Int() (i int64, err error) {
	return fastfloat.ParseInt64(string(v))
}

func (v stringValue) Uint() (u uint64, err error) {
	return fastfloat.ParseUint64(string(v))
}

func (v stringValue) Float() (f float64, err error) {
	return fastfloat.Parse(string(v))
}

func (v stringValue) Bool() (b bool, err error) {
	return parseBool(string(v))
}

func (v stringValue) Time() (t time.Time, err error) {
	var s string
	if s, err = v.String(); err != nil {
		return
	}
	var ex error
	for _, layout := range timeLayouts {
		if t, ex = time.Parse(layout, s); ex != nil {
			err = errors.Multi(err, ex)
		} else {
			err = nil
			return
		}
	}
	return
}

func (v stringValue) Duration() (d time.Duration, err error) {
	if s, err := v.String(); err != nil {
		return d, err
	} else {
		return time.ParseDuration(s)
	}
}

func (v stringValue) File() (f *multipart.FileHeader, err error) {
	return nil, ErrNoValue
}

func (v stringValue) Files() (f []*multipart.FileHeader, err error) {
	return nil, ErrNoValue
}

var _ Source = stringValue("")
