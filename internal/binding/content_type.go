package binding

import (
	"net/http"
	"strings"
)

func GetRequestContentType(req *http.Request) string {
	contentType := req.Header.Get("Content-Type")
	switch {
	case strings.HasPrefix(contentType, "application/x-www-form-urlencoded"):
		return ApplicationForm
	case strings.HasPrefix(contentType, "multipart/form-data"):
		return MultipartForm
	case strings.HasPrefix(contentType, "application/json"):
		return ApplicationJSON
	case strings.HasPrefix(contentType, "text/json"):
		return ApplicationJSON
	case strings.HasPrefix(contentType, "text/xml"):
		return ApplicationXML
	case strings.HasPrefix(contentType, "application/xml"):
		return ApplicationXML
	default:
		return ApplicationForm
	}
}
