package hs

import (
	"net/http"
)

type Option interface {
	Apply(*http.Server)
}

type ApplyFn func(s *http.Server)

func (apply ApplyFn) Apply(s *http.Server) {
	apply(s)
}
